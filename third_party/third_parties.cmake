add_library(rapidjson INTERFACE)
target_include_directories(rapidjson SYSTEM INTERFACE ${CMAKE_CURRENT_LIST_DIR}/rapidjson-1.1.0/include)
target_compile_definitions(rapidjson INTERFACE
	RAPIDJSON_HAS_STDSTRING
	_SILENCE_CXX17_ITERATOR_BASE_CLASS_DEPRECATION_WARNING
)

add_library(doctest INTERFACE)
target_include_directories(rapidjson SYSTEM INTERFACE ${CMAKE_CURRENT_LIST_DIR}/doctest-2.0.0/include)
