#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

#include "item1.h"

static const char * jsonText = R"(
{
    "strVal": "a long text with spaces",
    "intVal": -10,
    "uintVal": 20,
    "floatVal": -30.1,
    "doubleVal": 40.5,
    "intVector": [1,2,3],
    "strVector": [""],
    "pairVal": [50, "ok"],
    "optionalStr": "optional ok",
    "dummyField": "to be ignored",
    "unorderedMapStr": { "field1": "value1", "field2": "value2" },
    "unorderedMapInt": { "field1": 10 }
})";

TEST_CASE("item1 decoding") {
    rapidjson::Document doc;
    doc.Parse(jsonText);

    Item1 item;
    REQUIRE_NOTHROW(decode(doc, item));

    CHECK(item.strVal == "a long text with spaces");
    CHECK(item.intVal == -10);
    CHECK(item.uintVal == 20);
    CHECK(item.floatVal == -30.1);
    CHECK(item.doubleVal == 40.5);

    REQUIRE(item.intVector.size() == 3);
    CHECK(item.intVector[0] == 1);
    CHECK(item.intVector[1] == 2);
    CHECK(item.intVector[2] == 3);

    REQUIRE(item.strVector.size() == 1);
    CHECK(item.strVector[0] == "");

    CHECK(item.pairVal.first == 50);
    CHECK(item.pairVal.second == "ok");
#if EASYJSON_WITH_OPTIONAL
    CHECK(item.optionalStr.value_or("<missing>") == "optional ok");
#endif

    REQUIRE(item.unorderedMapStr.size() == 2);
    CHECK(item.unorderedMapStr["field1"] == "value1");
    CHECK(item.unorderedMapStr["field2"] == "value2");

    REQUIRE(item.unorderedMapInt.size() == 1);
    CHECK(item.unorderedMapInt["field1"] == 10);
}
