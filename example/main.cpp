#include "item1.h"

#include <easyjson.h>

#include <chrono>
#include <iostream>
#include <sstream>

static std::string generateJSON() {
    std::ostringstream oss;
    oss << R"(
{
    "strVal": "hello hjkhkjhj hkjhkjh jhkjhkjhjhkjh jkhkjhkjhkjh jkhkjhkjhjkhjkh jhkjhkjh jkhkjhkjhkjh jhkjh",
    "intVal": 10,
    "uintVal": 20,
    "floatVal": 30.1,
    "doubleVal": 40.5,
    "intVector": [1,2,3],
    "strVector": ["val1", "val2", "val3"],
    "pairVal": [50, "ok"],
    "optionalStr": "optional Ok",
    "dummyField": "to be ignored",
    "unorderedMapStr": { "field1": "value1" },
    "unorderedMapInt": { "field1": 10 }
})";
    return oss.str();
}

template <typename T>
size_t timeDiff(T start, T end) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
}

int main() {
    rapidjson::Document doc;
    doc.Parse(generateJSON());

    try {
        Item1 item;

        auto startTime = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < 10'000; i++) {
            decode(doc, item);
        }
        auto endTime = std::chrono::high_resolution_clock::now();
        std::cout << "Exec time: " << timeDiff(startTime, endTime) << " ms\n";

        // std::cout << items << "\n";
    } catch (const std::exception & e) {
        std::cerr << "Error: " << e.what() << "\n";
    }
}
