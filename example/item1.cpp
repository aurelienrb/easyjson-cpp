#include "item1.h"

#include <iomanip>

void decode(const easyjson::JsonValue & json, Item1 & value) {
    static const auto s_decoder = easyjson::makeDecoder("strVal", &Item1::strVal)
                                      .add("intVal", &Item1::intVal)
                                      .add("uintVal", &Item1::uintVal)
                                      .add("floatVal", &Item1::floatVal)
                                      .add("doubleVal", &Item1::doubleVal)
                                      .add("strVector", &Item1::strVector)
                                      .add("intVector", &Item1::intVector)
                                      .add("pairVal", &Item1::pairVal)
#if EASYJSON_WITH_OPTIONAL
                                      .add("optionalStr", &Item1::optionalStr)
#endif
                                      .add("unorderedMapStr", &Item1::unorderedMapStr)
                                      .add("unorderedMapInt", &Item1::unorderedMapInt)
                                      .build();
    s_decoder(json, value);
}

std::ostream & operator<<(std::ostream & stream, const Item1 & item) {
    stream << std::quoted(item.strVal) << "\n";
    stream << item.intVal << "\n";
    stream << item.uintVal << "\n";
    stream << item.floatVal << "\n";
    stream << item.doubleVal << "\n";

    stream << "[";
    for (size_t i = 0; i < item.strVector.size(); i++) {
        if (i != 0) {
            stream << ", ";
        }
        stream << std::quoted(item.strVector[i]);
    }
    stream << "]\n";

    stream << "[";
    for (size_t i = 0; i < item.intVector.size(); i++) {
        if (i != 0) {
            stream << ", ";
        }
        stream << item.intVector[i];
    }
    stream << "]\n";

    stream << "<" << item.pairVal.first << ", " << std::quoted(item.pairVal.second) << ">\n";

#if EASYJSON_WITH_OPTIONAL
    if (item.optionalStr) {
        stream << std::quoted(*item.optionalStr) << "\n";
    }
#endif

    for (auto it : item.unorderedMapStr) {
        stream << "- " << it.first << ": " << it.second << "\n";
    }

    for (auto it : item.unorderedMapInt) {
        stream << "- " << it.first << ": " << it.second << "\n";
    }

    return stream;
}
