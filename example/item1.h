#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <ostream>

#include <easyjson.h>

struct Item1 {
    std::string strVal;
    int intVal = 0;
    size_t uintVal = 0;
    double floatVal = 0.0f;
    double doubleVal = 0.0;
    std::vector<std::string> strVector;
    std::vector<int> intVector;
    std::pair<int, std::string> pairVal;
#if EASYJSON_WITH_OPTIONAL
    easyjson::Optional<std::string> optionalStr;
#endif
    std::unordered_map<std::string, std::string> unorderedMapStr;
    std::unordered_map<std::string, int> unorderedMapInt;
};

void decode(const easyjson::JsonValue & json, Item1 & value);

std::ostream & operator<<(std::ostream & stream, const Item1 & item);
