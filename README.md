# EasyJSON

[![pipeline status](https://gitlab.com/aurelienrb/easyjson-cpp/badges/master/pipeline.svg)](https://gitlab.com/aurelienrb/easyjson-cpp/commits/master)

## A C++ library that simplifies the use of other JSON parsers

Characteristics:
- header-only library that supports VC++, g++, clang
- based on top of [RapidJSON](https://github.com/Tencent/rapidjson) which is both very fast and reliable
- compatible with many standard C++ types: `vector`, `unordered_map`, `optional`
