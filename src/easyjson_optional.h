#pragma once

// To disable support for optional<> type:
//     #define EASYJSON_WITH_OPTIONAL    0
//
// To use an alternative optional type such as boost::optional<> or absl::optional<>:
//     #define EASYJSON_WITH_OPTIONAL    1
//     #define EASYJSON_OPTIONAL_INCLUDE <boost/optional.hpp>
//     #define EASYJSON_OPTIONAL_TYPE    ::boost::optional

#ifndef EASYJSON_WITH_OPTIONAL
// We can't just rely on __has_include() to decide if we should include <optional> or <experimental/optional>
// because both will be found on many compilers, but <optional> can only be included if compiling in C++17 mode
#    ifdef _MSC_VER
// to use std::optional<>, VC++ must be invoked with /std:c++17
#        if defined(_MSVC_LANG) && _MSVC_LANG >= 201700
#            define EASYJSON_OPTIONAL_INCLUDE <optional>
#            define EASYJSON_OPTIONAL_TYPE ::std::optional
#        endif
#    else
// The libstdc++ may support <optional> header, but the compiler must be invoked in C++17 mode to include it
// Note that gcc <= 6 will set __cplusplus to 2015xx even when compiling with "-std=c++1z" or "-std=c++17"
// and in such case <optional> can't be included even if available
#        if __cplusplus >= 201700 && __has_include(<optional>)
#            define EASYJSON_OPTIONAL_INCLUDE <optional>
#            define EASYJSON_OPTIONAL_TYPE ::std::optional
#        elif defined(__has_include) && __has_include(<experimental/optional>)
#            define EASYJSON_OPTIONAL_INCLUDE <experimental/optional>
#            define EASYJSON_OPTIONAL_TYPE ::std::experimental::optional
#        endif
#    endif
#endif

#if defined(EASYJSON_OPTIONAL_INCLUDE) && defined(EASYJSON_OPTIONAL_TYPE)
#    include EASYJSON_OPTIONAL_INCLUDE

namespace easyjson {
    template <typename T>
    using Optional = EASYJSON_OPTIONAL_TYPE<T>;
}

#    define EASYJSON_WITH_OPTIONAL 1
#endif
