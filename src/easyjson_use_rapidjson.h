#pragma once

#include <rapidjson/document.h>

#include <string>
#include <vector>

namespace easyjson {
    using JsonValue = rapidjson::GenericValue<rapidjson::UTF8<>>;

    inline std::string jsonTypeToString(const JsonValue & json) {
        // use the types defined in JSON Schema:
        // https://json-schema.org/understanding-json-schema/reference
        if (json.IsNull()) {
            return "null";
        } else if (json.IsNumber()) {
            if (json.IsUint() || json.IsUint64()) {
                return "positive integer";
            } else if (json.IsInt() || json.IsInt64()) {
                return "integer";
            }
            return "number";
        } else if (json.IsBool()) {
            return "boolean";
        } else if (json.IsString()) {
            return "string";
        } else if (json.IsArray()) {
            return "array of size " + std::to_string(json.GetArray().Size());
        } else if (json.IsObject()) {
            return "object";
        }
        return "unkown type";
    }

    struct ParsingContext {
        const char * currentFieldname = nullptr;
        const ParsingContext * parent = nullptr;
        const char * expected         = nullptr;

        std::string currentPath() const {
            const ParsingContext * ctx = this;
            std::string str{ ctx->currentFieldname };

            while (ctx->parent) {
                ctx = ctx->parent;
                str = std::string{ ctx->currentFieldname } + "." + str;
            }
            return str;
        }

        std::string makeErrorMsg(const std::string & details) const {
            std::string msg;
            msg += "[";
            msg += currentPath();
            msg += "]: ";
            msg += details;
            return msg;
        }

        std::string makeInvalidTypeError(const JsonValue & json) const {
            std::string str = "invalid type";
            if (expected) {
                str += ", expecting " + std::string{ expected };
            }
            str += ", found " + jsonTypeToString(json);
            return makeErrorMsg(str);
        }
    };

    // exception type used to describe a parsing error
    class ParsingError : public std::runtime_error {
    public:
        ParsingError(ParsingContext ctx, const std::string & details) : runtime_error(ctx.makeErrorMsg(details)) {}
        ParsingError(ParsingContext ctx, const JsonValue & json) : runtime_error(ctx.makeInvalidTypeError(json)) {}
    };

    template <typename T>
    void decodeSimpleType(ParsingContext ctx, const JsonValue & json, T & result) {
        if (json.Is<T>()) {
            result = json.Get<T>();
        } else {
            throw ParsingError{ ctx, json };
        }
    }

    inline void decode(ParsingContext ctx, const JsonValue & json, int & value) {
        ctx.expected = "integer";
        decodeSimpleType(ctx, json, value);
    }

    inline void decode(ParsingContext ctx, const JsonValue & json, size_t & value) {
        ctx.expected = "positive integer";
        decodeSimpleType(ctx, json, value);
    }

    inline void decode(ParsingContext ctx, const JsonValue & json, float & value) {
        ctx.expected = "floating point number";
        decodeSimpleType(ctx, json, value);
    }

    inline void decode(ParsingContext ctx, const JsonValue & json, double & value) {
        ctx.expected = "floating point number";
        decodeSimpleType(ctx, json, value);
    }

    inline void decode(ParsingContext ctx, const JsonValue & json, std::string & value) {
        ctx.expected = "string";
        decodeSimpleType(ctx, json, value);
    }
} // namespace easyjson
