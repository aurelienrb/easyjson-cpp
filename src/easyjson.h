#pragma once

#include "easyjson_optional.h"
#include "easyjson_use_rapidjson.h"

#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <utility>

namespace easyjson {
    template <typename T, typename U>
    void decode(ParsingContext ctx, const JsonValue & json, std::pair<T, U> & value) {
        ctx.expected = "array of size 2";
        if (json.IsArray()) {
            const auto & data = json.GetArray();
            if (data.Size() == 2) {
                decode(ctx, data[0], value.first);
                decode(ctx, data[1], value.second);
                return; // ok
            }
        }
        throw ParsingError{ ctx, json };
    }

    // arrays
    template <typename T>
    void decode(ParsingContext ctx, const JsonValue & json, std::vector<T> & value) {
        ctx.expected = "array";
        if (json.IsArray()) {
            const auto & data = json.GetArray();
            value.reserve(data.Size());

            for (const JsonValue & v : data) {
                T t;
                decode(ctx, v, t);
                value.emplace_back(std::move(t));
            }
        } else {
            throw ParsingError{ ctx, json };
        }
    }

    template <class T>
    struct IsOptionalType : std::false_type {};

#if EASYJSON_WITH_OPTIONAL
    template <class T>
    struct IsOptionalType<Optional<T>> : std::true_type {};

    // optional
    template <typename T>
    void decode(ParsingContext ctx, const JsonValue & json, Optional<T> & value) {
        T t;
        decode(ctx, json, t);
        value = t;
    }
#endif

    // unordered_map
    template <typename T>
    void decode(ParsingContext ctx, const JsonValue & json, std::unordered_map<std::string, T> & value) {
        ctx.expected = "map";
        if (json.IsObject()) {
            value.clear();
            for (auto it = json.MemberBegin(); it != json.MemberEnd(); it++) {
                const char * childName = it->name.GetString();
                ParsingContext childCtx{ childName, &ctx };
                decode(childCtx, it->value, value[childName]);
            }
        } else {
            throw ParsingError{ ctx, json };
        }
    }

    template <typename T>
    struct TypeDecoder {
        using Fn = std::function<void(ParsingContext, const JsonValue &, T &)>;

        // JSON field name associated with this member data
        const char * fieldName = nullptr;
        // decoder function that will assign the member data from the given JSON value
        Fn decoderFn;
        // true if member value in JSON is optional
        bool isOptional = false;
    };

    template <typename T, typename U>
    TypeDecoder<T> makeTypeDecoder(const char * fieldName, U T::*member) {
        return {
            fieldName,
            [=](ParsingContext ctx, const JsonValue & json, T & t) {
                decode(ParsingContext{ fieldName, &ctx }, json, t.*member);
            },
            IsOptionalType<U>(),
        };
    }

    template <typename T>
    class Decoder {
    public:
        using Map            = std::unordered_map<std::string, TypeDecoder<T>>;
        using UnknownHandler = std::function<void(const std::string &, const JsonValue & json)>;

        Decoder(Map decoders, UnknownHandler handler) : m_decoders(std::move(decoders)), m_unknown(handler) {}

        void operator()(const JsonValue & json, T & value) const { decode(ParsingContext{}, json, value); }

        T decode(ParsingContext ctx, const JsonValue & json) const {
            T result{};
            decode(ctx, json, result);
            return result;
        }

        void decode(ParsingContext ctx, const JsonValue & json, T & result) const {
            ctx.expected = "object";
            if (!json.IsObject()) {
                throw ParsingError{ ctx, json };
            }

            std::unordered_set<std::string> decodedFields;

            // try to decode each child field we find
            for (auto it = json.MemberBegin(); it != json.MemberEnd(); it++) {
                // TODO: use string_view here
                const std::string childName{ it->name.GetString(), it->name.GetStringLength() };
                auto itDec = m_decoders.find(childName);
                if (itDec != m_decoders.end()) {
                    const TypeDecoder<T> & dec = itDec->second;
                    dec.decoderFn(ctx, it->value, result);

                    // remember this decoded field and check it's the first time
                    const auto insertedIt = decodedFields.insert(childName);
                    if (!insertedIt.second) {
                        // was already decoded!
                        throw ParsingError{ ctx, "field '" + childName + "' appears more than once" };
                    }
                } else if (m_unknown) {
                    // report fields we are not aware of
                    m_unknown(childName, it->value);
                }
            }

            // now check that mandatory fields have been decoded
            for (auto it : m_decoders) {
                const TypeDecoder<T> & dec = it.second;
                if (!dec.isOptional) {
                    // check this mandatory field was decoded
                    const std::string name{ dec.fieldName };
                    if (decodedFields.find(name) == decodedFields.end()) {
                        throw ParsingError{ ctx, "mandatory field '" + name + "' is missing" };
                    }
                }
            }
        }

    private:
        Map m_decoders;
        UnknownHandler m_unknown;
    };

    template <typename T>
    class DecoderBuilder {
    public:
        template <typename U>
        DecoderBuilder & add(const char * name, U T::*member) {
            // check for duplicates
            assert(m_map.find(name) == m_map.end());
            m_map[name] = makeTypeDecoder(name, member);
            return *this;
        }

        using UnknownHandler = std::function<void(const std::string &, const JsonValue & json)>;
        DecoderBuilder & setUnknownHandler(UnknownHandler handler) {
            m_unknown = handler;
            return *this;
        }

        Decoder<T> build() { return Decoder<T>{ std::move(m_map), m_unknown }; }

    private:
        typename Decoder<T>::Map m_map;
        UnknownHandler m_unknown;
    };

    template <typename T, typename U>
    DecoderBuilder<T> makeDecoder(const char * fieldName, U T::*member) {
        return DecoderBuilder<T>{}.add(fieldName, member);
    }
} // namespace easyjson
